import numpy as np
def float_bin(number, places=6):
    """
    convert the float result into binary string
    e.g. 0.5->"0100000"->32
    param number: float number
    param palces: number of bits
    return int8_t number
    """
    if np.isnan(number):
        number = 0
    source = float("{:.5f}".format(number))
    N_flag = True if source < 0 else False
    # define max and min range
    if abs(source) >= (4-1/2 ** (places-2)):
        source = (4-1/2 ** (places-2))
        source *= -1 if N_flag else 1
    # source = rescaling(source, 4-1/2 ** (places-2))
    result = float_to_int8(source)
    
    return result

def decimal_converter(num):
    """
    for getting digit
    param num: num for decimal conversion
    return: digit
    """
    while num > 1:
        num /= 10
    return num

def rescaling(input, limit):
    """
    rescale number from symmetric range 
    to right side of zero
    param input: number to rescale
    param limit: abs of boundary
    return output: rescaled number
    """
    input += limit
    output = input / 2
    return output
    
def float_to_int8(source, places=6):
    # to convert float into int8, skip the bianry string part
    source_abs = abs(source)
    answer = 0
    base = 4 - 2**(-places+2)
    resolution = base*2/(2**places-1)
    answer = (int)((source+base)/resolution)
    return answer

def int8_float(number, isWeights=False):
    """
    main process to decode message as numeric number
    e.g. int 32->0.5 in image data
    param isWeights: True: weights, False: image data
    return float number result
    """
    message = number
    sign_flag = 1
    if isWeights:
        if message > 2**7:
            sign_flag = -1
            message -= 2**7  #remove sign
        elif message > 2**6:
            sign_flag = 1
            message -= 2**6 #remove sign
        else:
            sign_flag = 0
    
        message *= 2**(-4)*sign_flag
    else:
        message *= 2**(-5)*sign_flag

    return message

def _float_bin(number, places=6):
    """
    convert the float result into binary string
    e.g. 0.5->"0100000"->32
    param number: float number
    param palces: number of bits
    return int8_t number
    """
    if np.isnan(number):
        number = 0
    result = 0
    source = float("{:.5f}".format(number))
    N_flag = True if source < 0 else False
    # define max and min range
    if abs(source) >= (4-1/2 ** (places-2)):
        source = (4-1/2 ** (places-2))
        source *= -1 if N_flag else 1
    # source = self.rescaling(source, 4-1/2 ** (places-2))
    result = float_to_int8(source, places)
    return result

def _float_to_int8(source, places=6):
    # to convert float into int8, skip the bianry string part
        answer = 0
        base = 4 - 2**(-places+2)
        resolution = base*2/(2**places-1)
        answer = (int)((source+base)/resolution)
        return answer


# for i in range(20):
    # input = np.sin(2*i)*3.9375
    # print("answer: ", float_bin(input))

# print("answer: ", int8_float(160,False))
i = 3.9375

number = _float_bin(i)
print("float ", i, ": ", number)

i = 64+63

print("int: ", int8_float(i, False))


