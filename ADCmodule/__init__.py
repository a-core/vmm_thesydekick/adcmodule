"""
=========
ADCmodule
=========

ADCmodule model template The System Development Kit
Used as a template for all TheSyDeKick Entities.

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

This text here is to remind you that documentation is important.
However, youu may find it out the even the documentation of this 
entity may be outdated and incomplete. Regardless of that, every day 
and in every way we are getting better and better :).

Initially written by Marko Kosunen, marko.kosunen@aalto.fi, 2017.

"""

import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *

import numpy as np

class ADC(thesdk):
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self,*arg): 
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 
        sys.stdout.flush()
        self.proplist = [ 'Rs' ];    # Properties that can be propagated from parent
        self.Rs =  100e6;            # Sampling frequency
        self.IOS=Bundle()            # Pointer for input data
        self.IOS.Members['d_out']=IO()   # Pointer for output data
        self.IOS.Members['d_in']=IO()   # Pointer for input data

        self.IOS.Members['din_00'] = IO()   # Pointer for output data
        self.IOS.Members['din_01'] = IO()
        self.IOS.Members['din_02'] = IO()
        self.IOS.Members['din_03'] = IO()
        self.IOS.Members['din_04'] = IO()
        self.IOS.Members['din_05'] = IO()
        self.IOS.Members['din_06'] = IO()
        self.IOS.Members['din_07'] = IO()
        self.IOS.Members['din_08'] = IO()
        self.IOS.Members['din_09'] = IO()
        self.IOS.Members['din_10'] = IO()
        self.IOS.Members['din_11'] = IO()
        self.IOS.Members['din_12'] = IO()
        self.IOS.Members['din_13'] = IO()
        self.IOS.Members['din_14'] = IO()
        self.IOS.Members['din_15'] = IO()
        self.IOS.Members['din_16'] = IO()
        self.IOS.Members['din_17'] = IO()
        self.IOS.Members['din_18'] = IO()
        self.IOS.Members['din_19'] = IO()
        self.IOS.Members['din_20'] = IO()
        self.IOS.Members['din_21'] = IO()
        self.IOS.Members['din_22'] = IO()
        self.IOS.Members['din_23'] = IO()
        self.IOS.Members['din_24'] = IO()
        self.IOS.Members['din_25'] = IO()
        self.IOS.Members['din_26'] = IO()
        self.IOS.Members['din_27'] = IO()
        self.IOS.Members['din_28'] = IO()
        self.IOS.Members['din_29'] = IO()
        self.IOS.Members['din_30'] = IO()
        self.IOS.Members['din_31'] = IO()



        self.IOS.Members['adc_00'] = IO()   # Pointer for output data
        self.IOS.Members['adc_01'] = IO()
        self.IOS.Members['adc_02'] = IO()
        self.IOS.Members['adc_03'] = IO()
        self.IOS.Members['adc_04'] = IO()
        self.IOS.Members['adc_05'] = IO()
        self.IOS.Members['adc_06'] = IO()
        self.IOS.Members['adc_07'] = IO()
        self.IOS.Members['adc_08'] = IO()
        self.IOS.Members['adc_09'] = IO()
        self.IOS.Members['adc_10'] = IO()
        self.IOS.Members['adc_11'] = IO()
        self.IOS.Members['adc_12'] = IO()
        self.IOS.Members['adc_13'] = IO()
        self.IOS.Members['adc_14'] = IO()
        self.IOS.Members['adc_15'] = IO()
        self.IOS.Members['adc_16'] = IO()
        self.IOS.Members['adc_17'] = IO()
        self.IOS.Members['adc_18'] = IO()
        self.IOS.Members['adc_19'] = IO()
        self.IOS.Members['adc_20'] = IO()
        self.IOS.Members['adc_21'] = IO()
        self.IOS.Members['adc_22'] = IO()
        self.IOS.Members['adc_23'] = IO()
        self.IOS.Members['adc_24'] = IO()
        self.IOS.Members['adc_25'] = IO()
        self.IOS.Members['adc_26'] = IO()
        self.IOS.Members['adc_27'] = IO()
        self.IOS.Members['adc_28'] = IO()
        self.IOS.Members['adc_29'] = IO()
        self.IOS.Members['adc_30'] = IO()
        self.IOS.Members['adc_31'] = IO()

        self.test_flag = False

        self.model='py';             # Can be set externally, but is not propagated
        self.par= False              # By default, no parallel processing
        self.queue= []               # By default, no parallel processing

        if len(arg)>=1:
            parent=arg[0]
            self.copy_propval(parent,self.proplist)
            self.parent =parent

        self.init()

    def init(self):
        pass #Currently nohing to add

    def main(self):
        '''Guideline. Isolate python processing to main method.
        To isolate the interna processing from IO connection assigments, 
        The procedure to follow is
        1) Assign input data from input to local variable
        2) Do the processing
        3) Assign local variable to output
        '''
        
        for address in range(32):
            message_ = self.IOS.Members['din_{:02d}'.format(address)].Data
            data_ = self.float_to_bin(message_)
            self.IOS.Members['adc_{:02d}'.format(address)].Data = data_
        # if not self.test_flag:
        #     print("data{}@@@: ".format(0),
        #           self.IOS.Members['adc_00'].Data)
        #     self.test_flag = True


    def run(self,*arg):
        '''Guideline: Define model depencies of executions in `run` method.

        '''
        if len(arg)>0:
            self.par=True      #flag for parallel processing
            self.queue=arg[0]  #multiprocessing.queue as the first argument
        if self.model=='py':
            self.main()

    def __str__(self) -> str:
        return "this is ADC module"
        
        
        
    def float_to_bin(self, f_in):
        """
        Convert a float input (0 to 1) to a corresponding 6-bit integer value (0 to 63).
        If the input is out of range or NaN, it is clamped to the nearest valid value.
        """
        if np.isnan(f_in) or f_in < 0:
            f_in = 0
        elif f_in > 1:
            f_in = 1

        # Define the mapping values
        mapping_values = [
            0.0, 0.016, 0.032, 0.048, 0.063, 0.079, 0.095, 0.111, 0.127, 0.143, 0.159, 0.175, 0.19, 0.206, 0.222, 0.238,
            0.254, 0.27, 0.286, 0.302, 0.317, 0.333, 0.349, 0.365, 0.381, 0.397, 0.413, 0.429, 0.444, 0.46, 0.476, 0.492,
            0.508, 0.524, 0.54, 0.556, 0.571, 0.587, 0.603, 0.619, 0.635, 0.651, 0.667, 0.683, 0.698, 0.714, 0.73, 0.746,
            0.762, 0.778, 0.794, 0.81, 0.825, 0.841, 0.857, 0.873, 0.889, 0.905, 0.921, 0.937, 0.952, 0.968, 0.984, 1.0
                         ]

        # Find the closest value in the mapping
        closest_value = min(mapping_values, key=lambda x: abs(x - f_in))
        
        # Return the corresponding index
        return mapping_values.index(closest_value)
        


    def decimal_converter(self, num):
        """
        for getting digit
        param num: num for decimal conversion
        return: digit
        """
        while num > 1:
            num /= 10
        return num


    def rescaling(self, input, limit):
        """
        rescale number from symmetric range 
        to right side of zero
        param input: number to rescale
        param limit: abs of boundary
        return output: rescaled number
        """
        input += limit
        output = input / 2
        return output


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from  ADCmodule import *
    from  ADCmodule.controller import controller as ADCmodule_controller
    import pdb
    import math
    length=1024
    rs=100e6
    indata=np.cos(2*math.pi/length*np.arange(length)).reshape(-1,1)
    # range of vmm input filters

    models=[ 'py']
    duts=[]
    for model in models:
        d=ADC()
        duts.append(d) 
        d.model=model
        d.Rs=rs
        d.IOS.Members['A'].Data=indata
        d.init()
        d.run()

    input()

